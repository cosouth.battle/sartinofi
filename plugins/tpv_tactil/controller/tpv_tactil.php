<?php

/**
 * @author Carlos García Gómez      neorazorx@gmail.com
 * @copyright 2015-2016, Carlos García Gómez. All Rights Reserved.
 */

require_model('agente.php');
require_model('almacen.php');
require_model('articulo.php');
require_model('articulo_codbarras.php');
require_model('articulo_combinacion.php');
require_model('cliente.php');
require_model('divisa.php');
require_model('ejercicio.php');
require_model('fabricante.php');
require_model('factura_cliente.php');
require_model('familia.php');
require_model('forma_pago.php');
require_model('grupo_clientes.php');
require_model('impuesto.php');
require_model('pais.php');
require_model('recibo_cliente.php');
require_model('serie.php');
require_model('tarifa.php');
require_model('terminal_caja.php');
require_model('tpv_arqueo.php');
require_model('tpv_comanda.php');
require_model('tpv_movimiento.php');

class tpv_tactil extends fs_controller
{
   public $agente;
   public $almacen;
   public $arqueo;
   public $articulos_grid;
   public $busqueda;
   public $cliente;
   public $cliente_s;
   public $comanda;
   public $fabricante;
   public $forma_pago;
   public $historial;
   public $impuesto;
   public $movimiento;
   public $numlineas;
   public $resultado;
   public $serie;
   public $terminal;
   public $tesoreria;
   public $tpv_config;
   public $ultentregado;
   public $utlcambio;

   private $multi_almacen;

   public function __construct()
   {
      parent::__construct('tpv_tactil', 'Punto de venta', 'Puntos de venta');
   }

   protected function private_core()
   {
      $this->share_extensions();

      /// ¿El usuario tiene permiso para eliminar en esta página?
      $this->allow_delete = $this->user->allow_delete_on(__CLASS__);

      $this->agente = $this->user->get_agente();
      $this->almacen = new almacen();
      $this->arqueo = FALSE;
      $this->busqueda = '';
      $this->cliente = new cliente();
      $this->cliente_s = FALSE;
      $this->comanda = FALSE;
      $this->fabricante = new fabricante();
      $this->forma_pago = new forma_pago();
      $this->historial = array();
      $this->impuesto = new impuesto();
      $this->movimiento = FALSE;
      $this->numlineas = 0;
      $this->resultado = array();
      $this->serie = new serie();
      $this->terminal = FALSE;
      $this->tesoreria = class_exists('recibo_cliente');
      $this->utlcambio = 0;
      $this->ultentregado = 0;

      $fsvar = new fs_var();
      $this->multi_almacen = $fsvar->simple_get('multi_almacen');
      $this->tpv_config = array(
          'tpv_ref_varios' => '',
          'tpv_linea_libre' => 1,
          'tpv_familias' => FALSE,
          'tpv_fpago_efectivo' => FALSE,
          'tpv_fpago_tarjeta' => FALSE,
          'tpv_texto_fin' => ''
      );
      $this->tpv_config = $fsvar->array_get($this->tpv_config, FALSE);

      $this->articulos_grid = '6x3';
      if( isset($_COOKIE['tpv_tactil_articulos_grid']) )
      {
         $this->articulos_grid = $_COOKIE['tpv_tactil_articulos_grid'];
      }

      if( isset($_REQUEST['buscar_cliente']) )
      {
         $this->buscar_cliente();
      }
      else if( isset($_REQUEST['codbar2']) )
      {
         $this->buscar_articulo();
      }
      else if($this->query != '')
      {
         $this->new_search();
      }
      else if( isset($_REQUEST['codfamilia']) )
      {
         $this->get_articulos_familia();
      }
      else if( isset($_POST['referencia4combi']) )
      {
         $this->get_combinaciones_articulo();
      }
      else if( isset($_REQUEST['get_factura']) )
      {
         $this->get_factura();
      }
      else if( isset($_REQUEST['add_ref']) )
      {
         $this->add_ref();
      }
      else if($this->agente)
      {
         $arqueo = new tpv_arqueo();
         $terminal0 = new terminal_caja();
         foreach($arqueo->all_by_agente($this->agente->codagente) as $aq)
         {
            if($aq->abierta)
            {
               $this->arqueo = $aq;
               $this->terminal = $terminal0->get($aq->idterminal);
               break;
            }
         }

         if(!$this->arqueo)
         {
            if( isset($_POST['terminal']) )
            {
               $this->terminal = $terminal0->get($_POST['terminal']);
               if(!$this->terminal)
               {
                  $this->new_error_msg('Terminal no encontrado.');
               }
               else if( $this->terminal->disponible() )
               {
                  $this->arqueo = new tpv_arqueo();
                  $this->arqueo->idterminal = $this->terminal->id;
                  $this->arqueo->codagente = $this->agente->codagente;
                  $this->arqueo->inicio = floatval($_POST['d_inicial']);
                  $this->arqueo->totalcaja = floatval($_POST['d_inicial']);

                  if( $this->arqueo->save() )
                  {
                     $this->new_message("Arqueo iniciado con ".$this->show_precio($this->arqueo->inicio) );
                  }
                  else
                     $this->new_error_msg("¡Imposible guardar los datos del arqueo!");
               }
               else
                  $this->new_error_msg('El terminal ya no está disponible.');
            }
            else if( isset($_GET['terminal']) )
            {
               $this->terminal = $terminal0->get($_GET['terminal']);
               if($this->terminal)
               {
                  $this->terminal->abrir_cajon();
                  $this->terminal->save();
               }
               else
                  $this->new_error_msg('Terminal no encontrado.');
            }
         }

         if($this->arqueo)
         {
            if( isset($_POST['cliente']) )
            {
               $this->cliente_s = $this->cliente->get($_POST['cliente']);
            }
            else if($this->terminal)
            {
               $this->cliente_s = $this->cliente->get($this->terminal->codcliente);
            }

            if(!$this->cliente_s)
            {
               foreach($this->cliente->all() as $cli)
               {
                  $this->cliente_s = $cli;
                  break;
               }
            }

            if($this->cliente_s)
            {
               $this->caja_iniciada();
            }
            else
            {
               $this->new_error_msg('No hay ningún cliente. Crea uno, por ejemplo <b>Contado</b>.');
            }
         }
         else
         {
            $this->results = $terminal0->disponibles();
         }
      }
      else
      {
         $this->new_error_msg('No tienes un <a href="'.$this->user->url().'">agente asociado</a>
            a tu usuario, y por tanto no puedes hacer tickets.');
      }
   }

   private function caja_iniciada()
   {
      $this->template = 'tpv_tactil2';

      if( isset($_REQUEST['delete_comanda']) )
      {
         $tpvcom = new tpv_comanda();
         $comanda = $tpvcom->get($_REQUEST['delete_comanda']);
         if($comanda)
         {
            if( $comanda->delete() )
            {
               $this->new_message('Ticket eliminado correctamente.');
            }
         }
      }
      else if( isset($_REQUEST['idtpv_comanda']) )
      {
         $tpvcom = new tpv_comanda();
         $this->comanda = $tpvcom->get($_REQUEST['idtpv_comanda']);
         if($this->comanda)
         {
            $this->cliente_s = $this->cliente->get($this->comanda->codcliente);
         }
      }
      else if( isset($_GET['articulos_grid']) )
      {
         $this->articulos_grid = $_GET['articulos_grid'];
         setcookie('tpv_tactil_articulos_grid', $this->articulos_grid, time()+FS_COOKIES_EXPIRE);
      }

      if( isset($_GET['abrir_caja']) )
      {
         $this->abrir_caja();
      }
      else if( isset($_GET['cerrando']) )
      {
         $this->template = 'tpv_tactil_cierre';
         $this->terminal->abrir_cajon();
         $this->terminal->save();
      }
      else if( isset($_POST['cerrar_caja']) )
      {
         $this->cerrar_caja();
      }
      else if( isset($_POST['idfactura']) )
      {
         /// modificar una factura
         $this->modificar_factura();
      }
      else if( isset($_POST['cliente']) )
      {
         if($this->comanda)
         {
            $this->comanda->delete();
            $this->comanda = FALSE;
         }

         $this->guardar_ticket();
      }
      else if( isset($_GET['imprimir']) )
      {
         $fact0 = new factura_cliente();
         $factura = $fact0->get($_GET['imprimir']);
         if($factura)
         {
            $this->imprimir_ticket($factura);
         }
      }

      $comanda = new tpv_comanda();
      $this->historial = $comanda->all_from_arqueo($this->arqueo->idtpv_arqueo);

      foreach($this->historial as $h)
      {
         $this->ultentregado = $h->ultentregado;
         $this->utlcambio = $h->ultcambio;
         break;
      }
   }

   private function abrir_caja()
   {
      $this->terminal->abrir_cajon();

      if( isset($_POST['cantidad']) )
      {
         $mov = new tpv_movimiento();
         $mov->idtpv_arqueo = $this->arqueo->idtpv_arqueo;
         $mov->codagente = $this->user->codagente;

         if($_POST['movimiento'] == 'entrada')
         {
            $mov->cantidad = floatval($_POST['cantidad']);
         }
         else
         {
            $mov->cantidad = 0 - floatval($_POST['cantidad']);
         }

         $mov->descripcion = $_POST['descripcion'];

         if( $mov->save() )
         {
            $this->new_message('Movimiento guardado correctamente.');
            $this->arqueo->totalmov += $mov->cantidad;
            $this->arqueo->totalcaja += $mov->cantidad;
            $this->arqueo->save();
         }
         else
            $this->new_error_msg('Error al guardar el movimiento de caja.');
      }
      else
      {
         $this->movimiento = new tpv_movimiento();
      }

      $this->terminal->save();
   }

   private function cerrar_caja()
   {
      $this->arqueo->abierta = FALSE;
      $this->arqueo->diahasta = Date('d-m-Y');
      $this->arqueo->m001 = floatval($_POST['m1c']);
      $this->arqueo->m002 = floatval($_POST['m2c']);
      $this->arqueo->m005 = floatval($_POST['m5c']);
      $this->arqueo->m010 = floatval($_POST['m10c']);
      $this->arqueo->m020 = floatval($_POST['m20c']);
      $this->arqueo->m050 = floatval($_POST['m50c']);
      $this->arqueo->m1 = floatval($_POST['m1e']);
      $this->arqueo->m2 = floatval($_POST['m2e']);
      $this->arqueo->b5 = floatval($_POST['b5e']);
      $this->arqueo->b10 = floatval($_POST['b10e']);
      $this->arqueo->b20 = floatval($_POST['b20e']);
      $this->arqueo->b50 = floatval($_POST['b50e']);
      $this->arqueo->b100 = floatval($_POST['b100e']);
      $this->arqueo->b200 = floatval($_POST['b200e']);
      $this->arqueo->b500 = floatval($_POST['b500e']);

      if( $this->arqueo->save() )
      {
         $this->terminal->abrir_cajon();
         $this->terminal->add_linea("\n== ZETA / CIERRE DE CAJA ==\n");
         $this->terminal->add_linea($this->empresa->nombrecorto.' '.$this->terminal->nombre."\n");
         $this->terminal->add_linea("Cierre de caja: ".$this->arqueo->idtpv_arqueo."\n");
         $this->terminal->add_linea("Realizado: ".$this->today().' '.$this->hour()."\n");
         $this->terminal->add_linea("Cajero/a: ".$this->agente->get_fullname()."\n");

         $this->terminal->add_linea("\n== DINERO EN CAJA ==\n");
         $this->terminal->add_linea("Apertura: ".$this->arqueo->diadesde."\n");
         $this->terminal->add_linea("Inicial: ".$this->show_precio($this->arqueo->inicio, FALSE, FALSE)."\n");
         $this->terminal->add_linea("Ventas en efectivo: ".$this->show_precio($this->arqueo->totalcaja-$this->arqueo->inicio, FALSE, FALSE)."\n");
         $this->terminal->add_linea("------------------------\n");
         $this->terminal->add_linea("Efectivo en caja: ".$this->show_precio($this->arqueo->totalcaja, FALSE, FALSE)."\n");

         $this->terminal->add_linea("\n== VENTAS ==\n");
         $this->terminal->add_linea("Operaciones: ".$this->arqueo->num_tickets()."\n");
         $this->terminal->add_linea("Articulos: ".$this->arqueo->num_articulos()."\n");
         $this->terminal->add_linea("Unidades: ".$this->arqueo->count_articulos()."\n");
         $this->terminal->add_linea("Pagos en efectivo: ".$this->show_precio($this->arqueo->totalcaja-$this->arqueo->inicio, FALSE, FALSE)."\n");
         $this->terminal->add_linea("Pagos con tarjeta: ".$this->show_precio($this->arqueo->totaltarjeta, FALSE, FALSE)."\n");
         $this->terminal->add_linea("------------------------\n");
         $this->terminal->add_linea(
                 "Total ventas: ".
                 $this->show_precio($this->arqueo->totalcaja-$this->arqueo->inicio+$this->arqueo->totaltarjeta, FALSE, FALSE)."\n"
         );

         $this->terminal->add_linea("\n== ARQUEO DE CAJA ==\n");
         $this->terminal->add_linea("Efectivo en caja teorico: ".$this->show_precio($this->arqueo->totalcaja, FALSE, FALSE)."\n");
         $this->terminal->add_linea("Efectivo contado: ".$this->show_precio($this->arqueo->total_contado(), FALSE, FALSE)."\n");

         $diferencia = $this->arqueo->total_contado() - $this->arqueo->totalcaja;
         $this->terminal->add_linea("Diferencia: ".$this->show_precio($diferencia, FALSE, FALSE)."\n");

         $this->terminal->add_linea("\n== VENTAS POR Categoria ==\n");
         foreach($this->arqueo->desglose_familias() as $key => $fam)
         {
            $this->terminal->add_linea($fam['nombre'].' '.$this->show_numero($fam['cantidad'])."\n");
         }

         $this->terminal->add_linea("\n\n\n\n\n\n");
         $this->terminal->cortar_papel();
         $this->terminal->save();

         /// recargamos la página
         header('location: '.$this->url().'&terminal='.$this->terminal->id);
      }
      else
         $this->new_error_msg("¡Imposible cerrar la caja!");
   }

   private function buscar_cliente()
   {
      /// desactivamos la plantilla HTML
      $this->template = FALSE;

      $json = array();
      foreach($this->cliente->search($_REQUEST['buscar_cliente']) as $cli)
      {
         $json[] = array('value' => $cli->nombre, 'data' => $cli->codcliente);
      }

      header('Content-Type: application/json');
      echo json_encode( array('query' => $_REQUEST['buscar_cliente'], 'suggestions' => $json) );
   }

   private function buscar_articulo()
   {
      $this->template = 'ajax/tpv_tactil_lineas';
      $this->resultado = array();

      $articulo = new articulo();
      $artcod = new articulo_codbarras();
      foreach($artcod->search($_REQUEST['codbar2']) as $ac)
      {
         $this->resultado[] = $articulo->get($ac->referencia);
         break;
      }

      if( empty($this->resultado) )
      {
         foreach($articulo->search_by_codbar($_REQUEST['codbar2']) as $ac)
         {
            $this->resultado[] = $articulo->get($ac->referencia);
            break;
         }
      }

      $this->precios_resultados($this->resultado);

      $this->numlineas = 0;
      if( isset($_POST['numlineas']) )
      {
         $this->numlineas = $_POST['numlineas'];
      }
   }

   private function precios_resultados(&$res)
   {
      if($this->agente)
      {
         $arqueo = new tpv_arqueo();
         $terminal0 = new terminal_caja();
         foreach($arqueo->all_by_agente($this->agente->codagente) as $aq)
         {
            if($aq->abierta)
            {
               $this->arqueo = $aq;
               $this->terminal = $terminal0->get($aq->idterminal);
               break;
            }
         }
      }

      if($this->terminal)
      {
         $serie = $this->serie->get($this->terminal->codserie);
         $stock = new stock();
      }

      foreach($res as $i => $value)
      {
         $res[$i]->query = $this->query;
         $res[$i]->dtopor = 0;
         $res[$i]->cantidad = 1;

         $res[$i]->stockalm = $value->stockfis;
         if($this->terminal)
         {
            if($this->multi_almacen)
            {
               $res[$i]->stockalm = $stock->total_from_articulo($value->referencia, $this->terminal->codalmacen);
            }

            if($serie->siniva)
            {
               $res[$i]->codimpuesto = NULL;
            }
         }
      }

      if( isset($_REQUEST['codcliente']) )
      {
         $cliente = $this->cliente->get($_REQUEST['codcliente']);
         if($cliente)
         {
            if($cliente->regimeniva == 'Exento')
            {
               foreach($res as $i => $value)
               {
                  $res[$i]->codimpuesto = NULL;
               }
            }

            if($cliente->codgrupo)
            {
               $grupo0 = new grupo_clientes();
               $tarifa0 = new tarifa();

               $grupo = $grupo0->get($cliente->codgrupo);
               if($grupo)
               {
                  $tarifa = $tarifa0->get($grupo->codtarifa);
                  if($tarifa)
                  {
                     $tarifa->set_precios($res);

                     /// aplicamos el descuento al precio
                     foreach($res as $i => $value)
                     {
                        if($value->dtopor != 0)
                        {
                           $res[$i]->pvp -= $value->pvp * $value->dtopor / 100;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   private function get_articulos_familia()
   {
      $this->template = 'ajax/tpv_tactil_codfamilia';

      $familia = new familia();
      $fam = $familia->get($_REQUEST['codfamilia']);
      if($fam)
      {
         $this->resultado = $fam->get_articulos(0, 150);
         $this->precios_resultados($this->resultado);
      }
   }

   private function get_combinaciones_articulo()
   {
      /// cambiamos la plantilla HTML
      $this->template = 'ajax/tpv_tactil_combinaciones';

      $this->results = array();

      /// obtenemos el artículo
      $art0 = new articulo();
      $articulo = $art0->get($_POST['referencia4combi']);
      if($articulo)
      {
         /// usamos precios_resultados para obtener el precio de tarifa
         $aux = array($articulo);
         $this->precios_resultados($aux);

         $comb1 = new articulo_combinacion();
         foreach($comb1->all_from_ref($_POST['referencia4combi']) as $com)
         {
            if( isset($this->results[$com->codigo]) )
            {
               $this->results[$com->codigo]['desc'] .= ', '.$com->nombreatributo.' - '.$com->valor;
               $this->results[$com->codigo]['txt'] .= ', '.$com->nombreatributo.' - '.$com->valor;
            }
            else
            {
               $this->results[$com->codigo] = array(
                   'ref' => $_POST['referencia4combi'],
                   'desc' => $aux[0]->descripcion." | ".$com->nombreatributo.' - '.$com->valor,
                   'pvp' => floatval($aux[0]->pvp) + $com->impactoprecio,
                   'dto' => floatval($aux[0]->dtopor),
                   'codimpuesto' => $aux[0]->codimpuesto,
                   'iva' => $aux[0]->get_iva(),
                   'cantidad' => 1,
                   'txt' => $com->nombreatributo.' - '.$com->valor
               );
            }
         }
      }
   }

   private function get_factura()
   {
      $this->template = 'ajax/tpv_tactil_factura';
      $fact0 = new factura_cliente();
      $this->resultado = $fact0->get($_REQUEST['get_factura']);
   }

   private function add_ref()
   {
      $this->template = 'ajax/tpv_tactil_lineas';
      $this->resultado = array();

      $art0 = new articulo();
      $articulo = $art0->get($_REQUEST['add_ref']);
      if($articulo)
      {
         $this->resultado[] = $articulo;
         $this->precios_resultados($this->resultado);

         if( isset($_POST['desc']) )
         {
            $this->resultado[0]->descripcion = base64_decode($_POST['desc']);
            $this->resultado[0]->pvp = floatval($_POST['pvp']);
            $this->resultado[0]->dtopor = floatval($_POST['dto']);
         }

         $this->numlineas = 0;
         if( isset($_POST['numlineas']) )
         {
            $this->numlineas = $_POST['numlineas'];
         }
      }
   }

   private function guardar_ticket()
   {
      $continuar = TRUE;

      $ej0 = new ejercicio();
      $ejercicio = $ej0->get_by_fecha($_POST['fecha']);
      if(!$ejercicio)
      {
         $this->new_error_msg('Ejercicio no encontrado.');
         $continuar = FALSE;
      }

      $serie0 = new serie();
      $serie = $serie0->get($_POST['serie']);
      if(!$serie)
      {
         $this->new_error_msg('Serie no encontrada.');
         $continuar = FALSE;
      }

      $forma_pago = $this->forma_pago->get($this->tpv_config['tpv_fpago_efectivo']);
      $forma_pago2 = FALSE;
      if( isset($_POST['tpv_tarjeta']) )
      {
         if( floatval($_POST['tpv_tarjeta']) > 0 )
         {
            if( floatval($_POST['tpv_efectivo']) > 0 )
            {
               $forma_pago2 = $this->forma_pago->get($this->tpv_config['tpv_fpago_tarjeta']);
            }
            else
            {
               $forma_pago = $this->forma_pago->get($this->tpv_config['tpv_fpago_tarjeta']);
            }
         }
      }

      if(!$forma_pago)
      {
         $this->new_error_msg('Forma de pago no encontrada.');
         $continuar = FALSE;
      }

      $div0 = new divisa();
      $divisa = $div0->get($this->empresa->coddivisa);
      if(!$divisa)
      {
         $this->new_error_msg('Divisa no encontrada.');
         $continuar = FALSE;
      }

      $cliente = $this->cliente->get($_POST['cliente']);
      if(!$cliente)
      {
         $this->new_error_msg('Cliente no encontrado.');
         $continuar = FALSE;
      }

      $comanda = new tpv_comanda();

      if( $this->duplicated_petition($_POST['petition_id']) )
      {
         $this->new_error_msg('Petición duplicada. Has hecho doble clic sobre el botón Guardar
               y se han enviado dos peticiones.');
         $continuar = FALSE;
      }

      if($continuar)
      {
         $comanda->idtpv_arqueo = $this->arqueo->idtpv_arqueo;
         $comanda->fecha = $_POST['fecha'];
         $comanda->codalmacen = $this->empresa->codalmacen;

         $comanda->codpago = $forma_pago->codpago;
         if($forma_pago2)
         {
            $comanda->codpago2 = $forma_pago2->codpago;
         }

         if( floatval($_POST['tpv_efectivo']) > 0 )
         {
            $comanda->totalpago = floatval($_POST['tpv_efectivo']);
            if( isset($_POST['tpv_tarjeta']) )
            {
               if( floatval($_POST['tpv_tarjeta']) > 0 )
               {
                  $comanda->totalpago2 = floatval($_POST['tpv_tarjeta']);
               }
            }
         }
         else if( isset($_POST['tpv_tarjeta']) )
         {
            $comanda->totalpago = floatval($_POST['tpv_tarjeta']);
         }

         $comanda->observaciones = $_POST['observaciones'];
         $comanda->numero2 = $_POST['numero2'];
         $comanda->porcomision = $this->agente->porcomision;
         $comanda->ultentregado = floatval($_POST['tpv_efectivo']);
         $comanda->ultcambio = floatval($_POST['tpv_cambio']);

         foreach($cliente->get_direcciones() as $d)
         {
            if($d->domfacturacion)
            {
               $comanda->codcliente = $cliente->codcliente;
               $comanda->cifnif = $cliente->cifnif;
               $comanda->nombrecliente = $cliente->razonsocial;
               $comanda->ciudad = $d->ciudad;
               $comanda->coddir = $d->id;
               $comanda->codpais = $d->codpais;
               $comanda->codpostal = $d->codpostal;
               $comanda->direccion = $d->direccion;
               $comanda->provincia = $d->provincia;
               break;
            }
         }

         if( is_null($comanda->codcliente) )
         {
            $this->new_error_msg("No hay ninguna dirección asociada al cliente.");
         }
         else if( $comanda->save() )
         {
            $articulo = new articulo();

            $n = floatval($_POST['numlineas']);
            for($i = 0; $i < $n; $i++)
            {
               if( isset($_POST['referencia_'.$i]) )
               {
                  $art0 = $articulo->get($_POST['referencia_'.$i]);
                  if($art0)
                  {
                     $linea = new linea_comanda();
                     $linea->idtpv_comanda = $comanda->idtpv_comanda;
                     $linea->referencia = $art0->referencia;
                     $linea->descripcion = $_POST['desc_'.$i];
                     $linea->codimpuesto = $art0->codimpuesto;
                     $linea->iva = floatval($_POST['iva_'.$i]);
                     $linea->pvpunitario = floatval($_POST['pvp_'.$i]);
                     $linea->cantidad = floatval($_POST['cantidad_'.$i]);
                     $linea->pvpsindto = ($linea->pvpunitario * $linea->cantidad);
                     $linea->pvptotal = ($linea->pvpunitario * $linea->cantidad);

                     if( $linea->save() )
                     {
                        /// descontamos del stock
                        $art0->sum_stock($comanda->codalmacen, 0 - $linea->cantidad);

                        $comanda->neto += $linea->pvptotal;
                        $comanda->totaliva += ($linea->pvptotal * $linea->iva/100);
                     }
                     else
                     {
                        $this->new_error_msg("¡Imposible guardar la línea con referencia: ".$linea->referencia);
                        $continuar = FALSE;
                     }
                  }
                  else
                  {
                     $this->new_error_msg("Artículo no encontrado: ".$_POST['referencia_'.$i]);
                     $continuar = FALSE;
                  }
               }
            }

            if($continuar)
            {
               /// redondeamos
               $comanda->neto = round($comanda->neto, FS_NF0);
               $comanda->totaliva = round($comanda->totaliva, FS_NF0);
               $comanda->total = $comanda->neto + $comanda->totaliva;

               if( abs(floatval($_POST['tpv_total']) - $comanda->total) >= .02 )
               {
                  $this->new_error_msg("El total difiere entre la vista y el controlador (".$_POST['tpv_total'].
                          " frente a ".$comanda->total."). Debes informar del error.");
                  $comanda->delete();
               }
               else if( $comanda->save() )
               {
                  if($_POST['aparcar'] == 'FALSE')
                  {
                     $this->generar_factura($comanda, $ejercicio, $serie, $divisa);
                  }
                  else
                  {
                     $this->new_message("Ticket aparcado.");
                  }
               }
               else
                  $this->new_error_msg("¡Imposible actualizar el ticket!");
            }
            else if( $comanda->delete() )
            {
               $this->new_message("Ticket eliminado correctamente.");
            }
            else
               $this->new_error_msg("¡Imposible eliminar el ticket!");
         }
         else
            $this->new_error_msg("¡Imposible guardar el ticket!");
      }
   }

   /**
    * Añade el ticket a la cola de impresión.
    * @param factura_cliente $factura
    */
   private function imprimir_ticket($factura, $numtickets = 1, $cajon = FALSE)
   {
      if($cajon)
      {
         $this->terminal->abrir_cajon();
      }

      while($numtickets > 0)
      {
         $this->terminal->logo();

         $linea_iguales = '';
         for($i = 0; $i < $this->terminal->anchopapel; $i++)
         {
            $linea_iguales .= '=';
         }
         $this->terminal->add_linea($linea_iguales."\n");

         $this->terminal->add_linea( $this->sanitize($this->empresa->nombrecorto)."\n" );
         $this->terminal->add_linea(
            $this->sanitize($this->empresa->direccion)."\nCP: ".$this->empresa->codpostal
                 .' - '.$this->sanitize($this->empresa->ciudad)."\n"
         );

         if($this->empresa->telefono)
         {
            $this->terminal->add_linea('Tlf: '.$this->empresa->telefono."\n\n");
         }

         $this->terminal->add_linea( $this->sanitize($this->empresa->nombre).'  ' );
         $this->terminal->add_linea(FS_CIFNIF.': '.$this->empresa->cifnif."\n");

         $this->terminal->add_linea($linea_iguales."\n");

         $this->terminal->add_linea(
                 $this->terminal->center_text( ucfirst(FS_FACTURA_SIMPLIFICADA).' '.$factura->codigo )."\n".
                 $this->terminal->center_text( $factura->fecha.' '.$factura->hora )."\n".
                 $this->terminal->center_text( $this->terminal->nombre )."\n\n"
         );

         $width = $this->terminal->anchopapel - 15;
         $this->terminal->add_linea(
                 sprintf("%3s", "Ud.")." ".
                 sprintf("%-".$width."s", "Articulo")." ".
                 sprintf("%10s", "TOTAL")."\n"
         );
         $this->terminal->add_linea(
                 sprintf("%3s", "---")." ".
                 sprintf("%-".$width."s", substr("--------------------------------------------------------", 0, $width-1))." ".
                 sprintf("%10s", "----------")."\n"
         );
         $impuestos = array();
         $totales = array();
         $num_articulos = 0;
         foreach($factura->get_lineas() as $col)
         {
            /// desglosamos las bases y el iva
            if( !in_array($col->iva, $impuestos) )
            {
               $impuestos[] = $col->iva;
               $totales[$col->iva] = array(
                   'neto' => $col->pvptotal,
                   'iva' => $col->iva,
                   'totaliva' => $col->pvptotal*$col->iva/100,
                   're' => $col->recargo,
                   'totalre' => $col->pvptotal*$col->recargo/100,
               );
            }
            else
            {
               $totales[$col->iva]['neto'] += $col->pvptotal;
               $totales[$col->iva]['totaliva'] += $col->pvptotal*$col->iva/100;
               $totales[$col->iva]['totalre'] += $col->pvptotal*$col->recargo/100;
            }

            $num_articulos += $col->cantidad;

            $this->terminal->add_linea(
                    sprintf("%3s", $col->cantidad)." ".$this->sanitize($col->referencia).":\n"
            );
            $this->terminal->add_linea(
                    '    '.substr( $this->sanitize($col->descripcion), 0, $this->terminal->anchopapel - 4)."\n"
            );
            $this->terminal->add_linea(
                    '    '.sprintf("%-".$width."s", 'PVP: '.$this->show_numero($col->pvpunitario*(100+$col->iva)/100))." ".
                    sprintf("%10s", $this->show_numero($col->total_iva()))."\n"
            );
         }

         $this->terminal->add_linea($linea_iguales."\n");
         $this->terminal->add_linea(
                 'TOTAL A PAGAR: '.sprintf("%".($this->terminal->anchopapel-15)."s", $this->show_precio($factura->total, $factura->coddivisa, FALSE))
         );
         $this->terminal->add_linea($linea_iguales."\n");

         $entregado = 0;
         if( isset($_POST['tpv_efectivo']) )
         {
            $entregado = floatval($_POST['tpv_efectivo']);
            $this->terminal->add_linea(
                 'Efectivo: '.
                 sprintf('%'.($this->terminal->anchopapel-10).'s', $this->show_precio($entregado, $factura->coddivisa, FALSE)).
                 "\n"
            );
         }

         if( isset($_POST['tpv_tarjeta']) )
         {
            $this->terminal->add_linea(
                 "Tarjeta: ".
                 sprintf('%'.($this->terminal->anchopapel-9).'s', $this->show_precio(floatval($_POST['tpv_tarjeta']), $factura->coddivisa, FALSE)).
                 "\n"
            );
            $entregado += floatval($_POST['tpv_tarjeta']);
         }

         if( isset($_POST['tpv_cambio']) )
         {
            $this->terminal->add_linea(
                 "Total entregado: ".
                 sprintf('%'.($this->terminal->anchopapel-17).'s', $this->show_precio($entregado, $factura->coddivisa, FALSE)).
                 "\n"
            );
            $this->terminal->add_linea(
                 "Cambio: ".
                 sprintf('%'.($this->terminal->anchopapel-8).'s', $this->show_precio(floatval($_POST['tpv_cambio']), $factura->coddivisa, FALSE)).
                 "\n"
            );
         }

         $agente = new agente();
         $age0 = $agente->get($factura->codagente);
         if($age0)
         {
            $this->terminal->add_linea(
                    "Le atendio: ".
                    sprintf('%'.($this->terminal->anchopapel-12).'s', $age0->nombre).
                    "\n"
            );
         }

         $this->terminal->add_linea(
                 "Num. de articulos: ".
                 sprintf('%'.($this->terminal->anchopapel-19).'s', $this->show_numero($num_articulos, 0)).
                 "\n"
         );

         $this->terminal->add_linea($linea_iguales."\n");

         /// imprimimos los impuestos desglosados
         $this->terminal->add_linea(
                 'TIPO   BASE    '.FS_IVA.'    RE'.
                 sprintf('%'.($this->terminal->anchopapel-24).'s', 'TOTAL').
                 "\n"
         );
         foreach($impuestos as $imp)
         {
            $total = $totales[$imp]['neto']+$totales[$imp]['totaliva']+$totales[$imp]['totalre'];
            $this->terminal->add_linea(
                    sprintf("%-6s", $imp.'%').' '.
                    sprintf("%-7s", $this->show_numero($totales[$imp]['neto'])).' '.
                    sprintf("%-6s", $this->show_numero($totales[$imp]['totaliva'])).' '.
                    sprintf("%-6s", $this->show_numero($totales[$imp]['totalre'])).' '.
                    sprintf('%'.($this->terminal->anchopapel-29).'s', $this->show_numero($total)).
                    "\n"
            );
         }

         $this->terminal->add_linea(
                 "\n\n".
                 $this->terminal->center_text('** Gracias por su visita **').
                 "\n\n".
                 $this->terminal->center_text( $this->sanitize($this->tpv_config['tpv_texto_fin']) )
         );

         $this->terminal->add_linea("\n\n\n\n\n\n\n");
         $this->terminal->cortar_papel();
         $numtickets--;
      }

      $this->terminal->save();
   }

   /**
    *
    * @param tpv_comanda $comanda
    * @param ejercicio $ejercicio
    * @param serie $serie
    * @param divisa $divisa
    */
   private function generar_factura(&$comanda, &$ejercicio, &$serie, &$divisa)
   {
      $factura = new factura_cliente();
      $factura->cifnif = $comanda->cifnif;
      $factura->ciudad = $comanda->ciudad;
      $factura->codagente = $this->agente->codagente;
      $factura->codalmacen = $comanda->codalmacen;
      $factura->codcliente = $comanda->codcliente;
      $factura->coddir = $comanda->coddir;
      $factura->coddivisa = $divisa->coddivisa;
      $factura->codejercicio = $ejercicio->codejercicio;
      $factura->codpago = $comanda->codpago;
      $factura->codpais = $comanda->codpais;
      $factura->codpostal = $comanda->codpostal;
      $factura->codserie = $serie->codserie;
      $factura->direccion = $comanda->direccion;
      $factura->neto = $comanda->neto;
      $factura->nombrecliente = $comanda->nombrecliente;
      $factura->numero2 = $comanda->numero2;
      $factura->observaciones = $comanda->observaciones;
      $factura->porcomision = $this->agente->porcomision;
      $factura->provincia = $comanda->provincia;
      $factura->tasaconv = $divisa->tasaconv;
      $factura->total = $comanda->total;
      $factura->totaliva = $comanda->totaliva;

      if( $factura->save() )
      {
         foreach($comanda->get_lineas() as $lin)
         {
            $linea = new linea_factura_cliente();
            $linea->idfactura = $factura->idfactura;
            $linea->cantidad = $lin->cantidad;
            $linea->codimpuesto = $lin->codimpuesto;
            $linea->descripcion = $lin->descripcion;
            $linea->dtopor = $lin->dtopor;
            $linea->irpf = $lin->irpf;
            $linea->iva = $lin->iva;
            $linea->pvpsindto = $lin->pvpsindto;
            $linea->pvptotal = $lin->pvptotal;
            $linea->pvpunitario = $lin->pvpunitario;
            $linea->recargo = $lin->recargo;
            $linea->referencia = $lin->referencia;
            $linea->save();
         }

         if($this->tesoreria)
         {
            if( floatval($_POST['tpv_efectivo']) > 0 AND floatval($_POST['tpv_tarjeta']) > 0 AND $this->tesoreria )
            {
               $this->generar_recibos($factura);
            }
         }

         $this->arqueo->totalcaja += floatval($_POST['tpv_efectivo']) - floatval($_POST['tpv_cambio']);
         if( isset($_POST['tpv_tarjeta']) )
         {
            $this->arqueo->totaltarjeta += floatval($_POST['tpv_tarjeta']);
         }
         $this->arqueo->save();

         $this->imprimir_ticket($factura, $this->terminal->num_tickets, TRUE);

         $comanda->idfactura = $factura->idfactura;
         $comanda->save();

         $this->new_message('<a href="'.$factura->url().'">Factura</a> guardada correctamente.');
      }
      else
      {
         $this->new_error_msg('Error al guardar la factura.');
      }
   }

   public function aux_articulos_tabs()
   {
      $tabs = array();
      $num = 18;
      if($this->articulos_grid == '4x3')
      {
         $num = 12;
      }
      else if($this->articulos_grid == '3x3')
      {
         $num = 9;
      }

      for($i = 0; $i*$num <= count($this->resultado); $i++)
      {
         $tabs[] = $i+1;
      }

      return $tabs;
   }

   public function aux_articulos_grid()
   {
      $num = 18;
      if($this->articulos_grid == '4x3')
      {
         $num = 12;
      }
      else if($this->articulos_grid == '3x3')
      {
         $num = 9;
      }

      $articulos = array();

      $num2 = 0;
      foreach($this->aux_articulos_tabs() as $tab)
      {
         $arttab = array();
         foreach($this->resultado as $i => $value)
         {
            if($i >= $num2 AND $i < $num2+$num)
            {
               $value->funcion_js = "return add_referencia('".urlencode($value->referencia)."')";
               if($value->tipo == 'atributos')
               {
                  $value->funcion_js = "return get_combinaciones('".urlencode($value->referencia)."')";
               }

               $arttab[] = $value;
            }
         }

         $articulos[$tab] = $arttab;
         $num2 += $num;
      }

      return $articulos;
   }

   public function familias($todas = FALSE)
   {
      $familias = array();

      $familia = new familia();
      if($todas)
      {
         $familias = $familia->all();
      }
      else if($this->tpv_config['tpv_familias'])
      {
         $aux = explode(',', $this->tpv_config['tpv_familias']);
         if($aux)
         {
            foreach($familia->all() as $fam)
            {
               if( in_array($fam->codfamilia, $aux) )
               {
                  $familias[] = $fam;
               }
            }
         }
      }

      return $familias;
   }

   private function modificar_factura()
   {
      $fact0 = new factura_cliente();
      $factura = $fact0->get($_POST['idfactura']);
      if($factura)
      {
         $articulo = new articulo();
         $asient0 = new asiento();

         /// paso 1, eliminamos el asiento asociado
         if( !is_null($factura->idasiento) )
         {
            $asiento = $asient0->get($factura->idasiento);
            if($asiento)
            {
               if( $asiento->delete() )
               {
                  $factura->idasiento = NULL;
               }
            }
            else
               $factura->idasiento = NULL;
         }

         /// paso 2, eliminamos las líneas
         foreach($factura->get_lineas() as $linea)
         {
            $linea->delete();
         }

         /// paso 3, eliminar la líneas de IVA
         foreach($factura->get_lineas_iva() as $liva)
         {
            $liva->delete();
         }

         /// paso 4, guardamos las nuevas
         $continuar = TRUE;
         $factura->neto = 0;
         $factura->totaliva = 0;
         $factura->totalirpf = 0;
         $factura->totalrecargo = 0;
         for($i = 1; $i < 200; $i++)
         {
            if( isset($_POST['f_referencia_'.$i]) )
            {
               $art0 = $articulo->get($_POST['f_referencia_'.$i]);
               if($art0)
               {
                  $linea = new linea_factura_cliente();
                  $linea->idfactura = $factura->idfactura;
                  $linea->referencia = $art0->referencia;
                  $linea->descripcion = $_POST['f_desc_'.$i];
                  $linea->codimpuesto = $art0->codimpuesto;
                  $linea->iva = floatval($_POST['f_iva_'.$i]);
                  $linea->pvpunitario = floatval($_POST['f_pvp_'.$i]);
                  $linea->cantidad = floatval($_POST['f_cantidad_'.$i]);
                  $linea->pvpsindto = ($linea->pvpunitario * $linea->cantidad);
                  $linea->pvptotal = ($linea->pvpunitario * $linea->cantidad);

                  if( $linea->save() )
                  {
                     /// descontamos del stock
                     $art0->sum_stock($factura->codalmacen, 0 - $linea->cantidad);

                     $factura->neto += $linea->pvptotal;
                     $factura->totaliva += ($linea->pvptotal * $linea->iva/100);
                  }
                  else
                  {
                     $this->new_error_msg("¡Imposible guardar la línea con referencia: ".$linea->referencia);
                     $continuar = FALSE;
                  }
               }
               else
               {
                  $this->new_error_msg("Artículo no encontrado: ".$_POST['f_referencia_'.$i]);
                  $continuar = FALSE;
               }
            }
         }

         if($continuar)
         {
            /// redondeamos
            $factura->neto = round($factura->neto, FS_NF0);
            $factura->totaliva = round($factura->totaliva, FS_NF0);
            $factura->total = $factura->neto + $factura->totaliva;

            if( $factura->save() )
            {
               $this->new_message('<a href="'.$factura->url().'" class="text-capitalize">'
                       .FS_FACTURA.'</a> modificada correctamente.');
            }
            else
               $this->new_error_msg("Error al modificar la factura.");
         }
         else
            $this->new_error_msg("Error al modificar la factura.");
      }
      else
      {
         $this->new_error_msg('Factura no encontrada.');
      }
   }

   /**
    *
    * @param factura_cliente $factura
    */
   private function generar_recibos($factura)
   {
      $formap = $this->forma_pago->get($this->tpv_config['tpv_fpago_efectivo']);
      if($formap)
      {
         $recibo = new recibo_cliente();
         $recibo->cifnif = $factura->cifnif;
         $recibo->coddivisa = $factura->coddivisa;
         $recibo->codcliente = $factura->codcliente;
         $recibo->estado = 'Emitido';
         if($formap->genrecibos == 'Pagados')
         {
            $recibo->estado = 'Pagado';
         }
         $recibo->fecha = $factura->fecha;
         $recibo->fechav = Date('d-m-Y', strtotime($factura->fecha.' '.$formap->vencimiento));
         $recibo->idfactura = $factura->idfactura;
         $recibo->importe = floatval($_POST['tpv_efectivo']);
         $recibo->codpago = $formap->codpago;
         $recibo->nombrecliente = $factura->nombrecliente;
         $recibo->numero = $recibo->new_numero($recibo->idfactura);
         $recibo->codigo = $factura->codigo.'-'.sprintf('%02s', $recibo->numero);
         $recibo->save();
      }

      $formap = $this->forma_pago->get($this->tpv_config['tpv_fpago_tarjeta']);
      if($formap)
      {
         $recibo = new recibo_cliente();
         $recibo->cifnif = $factura->cifnif;
         $recibo->coddivisa = $factura->coddivisa;
         $recibo->codcliente = $factura->codcliente;
         $recibo->estado = 'Emitido';
         if($formap->genrecibos == 'Pagados')
         {
            $recibo->estado = 'Pagado';
         }
         $recibo->fecha = $factura->fecha;
         $recibo->fechav = Date('d-m-Y', strtotime($factura->fecha.' '.$formap->vencimiento));
         $recibo->idfactura = $factura->idfactura;
         $recibo->importe = floatval($_POST['tpv_tarjeta']);
         $recibo->codpago = $formap->codpago;
         $recibo->nombrecliente = $factura->nombrecliente;
         $recibo->numero = $recibo->new_numero($recibo->idfactura);
         $recibo->codigo = $factura->codigo.'-'.sprintf('%02s', $recibo->numero);
         $recibo->save();
      }
   }

   private function share_extensions()
   {
      $fsext = new fs_extension();
      $fsext->name = 'api_remote_printer';
      $fsext->from = __CLASS__;
      $fsext->type = 'api';
      $fsext->text = 'remote_printer';
      $fsext->save();
   }

   private function new_search()
   {
      /// desactivamos la plantilla HTML
      $this->template = FALSE;
      $articulo = new articulo();
      $stock = new stock();

      $codfamilia = '';
      if( isset($_REQUEST['codfamilia']) )
      {
         $codfamilia = $_REQUEST['codfamilia'];
      }
      $codfabricante = '';
      if( isset($_REQUEST['codfabricante']) )
      {
         $codfabricante = $_REQUEST['codfabricante'];
      }
      $con_stock = isset($_REQUEST['con_stock']);
      $resultados = $articulo->search($this->query, 0, $codfamilia, $con_stock, $codfabricante);

      $this->precios_resultados($resultados);

      header('Content-Type: application/json');
      echo json_encode($resultados);
   }

   private function sanitize($txt)
   {
      $changes = array('/à/' => 'a', '/á/' => 'a', '/â/' => 'a', '/ã/' => 'a', '/ä/' => 'a',
          '/å/' => 'a', '/æ/' => 'ae', '/ç/' => 'c', '/è/' => 'e', '/é/' => 'e', '/ê/' => 'e',
          '/ë/' => 'e', '/ì/' => 'i', '/í/' => 'i', '/î/' => 'i', '/ï/' => 'i', '/ð/' => 'd',
          '/ñ/' => 'n', '/ò/' => 'o', '/ó/' => 'o', '/ô/' => 'o', '/õ/' => 'o', '/ö/' => 'o',
          '/ő/' => 'o', '/ø/' => 'o', '/ù/' => 'u', '/ú/' => 'u', '/û/' => 'u', '/ü/' => 'u',
          '/ű/' => 'u', '/ý/' => 'y', '/þ/' => 'th', '/ÿ/' => 'y',
          '/&quot;/' => '-',
          '/À/' => 'A', '/Á/' => 'A', '/Â/' => 'A', '/Ä/' => 'A',
          '/Ç/' => 'C', '/È/' => 'E', '/É/' => 'E', '/Ê/' => 'E',
          '/Ë/' => 'E', '/Ì/' => 'I', '/Í/' => 'I', '/Î/' => 'I', '/Ï/' => 'I',
          '/Ñ/' => 'N', '/Ò/' => 'O', '/Ó/' => 'O', '/Ô/' => 'O', '/Ö/' => 'O',
          '/Ù/' => 'U', '/Ú/' => 'U', '/Û/' => 'U', '/Ü/' => 'U',
          '/Ý/' => 'Y', '/Ÿ/' => 'Y',
      );
      return preg_replace(array_keys($changes), $changes, $txt);
   }
}
