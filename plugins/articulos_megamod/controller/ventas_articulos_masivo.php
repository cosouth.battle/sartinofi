<?php

require_model("articulo.php");
require_model("impuesto.php");
require_model("fabricante.php");
require_model("familia.php");
require_model("proveedor.php");
require_model('ps_articulo_categoria.php');
require_model("ps_categoria.php");

class ventas_articulos_masivo extends fs_controller
{
   public $articulo;
   public $codfabricante;
   public $codfamilia;
   public $codproveedor;
   public $codpscat;
   public $fabricante;
   public $familia;
   public $impuesto;
   public $num_resultados;
   public $offset;
   public $prestashop;
   public $proveedor;
   public $ps_categoria;
   public $resultados;
   public $subfamilias;
   public $tipo;

   public function __construct()
   {
      parent::__construct(__CLASS__, "Articulos Megamod", "ventas");
   }

   protected function private_core()
   {
      $this->share_extensions();
      $this->articulo = new articulo();
      $this->impuesto = new impuesto();
      $this->fabricante = new fabricante();
      $this->familia = new familia();
      $this->prestashop = in_array('prestashop', $GLOBALS['plugins']);
      $this->proveedor = new proveedor();

      $this->offset = 0;
      if( isset($_REQUEST['offset']) )
      {
         $this->offset = intval($_REQUEST['offset']);
      }

      $this->codfabricante = '';
      if( isset($_REQUEST['codfabricante']) )
      {
         $this->codfabricante = $_REQUEST['codfabricante'];
      }

      $this->codfamilia = '';
      $this->subfamilias = FALSE;
      if( isset($_REQUEST['codfamilia']) )
      {
         $this->codfamilia = $_REQUEST['codfamilia'];
         if($_REQUEST['codfamilia'] != '')
         {
            $this->subfamilias = isset($_REQUEST['subfamilias']);
         }
      }

      if($this->prestashop)
      {
         $this->ps_categoria = new ps_categoria();
         $this->codpscat = '';
         if( isset($_POST['codpscat']) )
         {
            $this->codpscat = $_POST['codpscat'];
         }
      }
      else
      {
         $this->ps_categoria = FALSE;
         $this->codpscat = '';
      }

      $this->codproveedor = '';
      if( isset($_POST['codproveedor']) )
      {
         $this->codproveedor = $_POST['codproveedor'];
      }

      $this->tipo = '';
      if( isset($_REQUEST['tipo']) )
      {
         $this->tipo = $_REQUEST['tipo'];
      }

      $this->aplicar_cambios();
      $this->buscar_articulos();
   }

   private function share_extensions()
   {
      $fsext = new fs_extension();
      $fsext->name = 'btn_modificar';
      $fsext->from = __CLASS__;
      $fsext->to = 'ventas_articulos';
      $fsext->type = 'button';
      $fsext->text = '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>'
              . '<span class="hidden-xs">&nbsp; Megamod</span>';
      $fsext->save();
   }

   private function buscar_articulos()
   {
      $this->num_resultados = 0;
      $this->resultados = array();

      $query = $this->articulo->no_html( strtolower($this->query) );

      if($this->codfamilia == '')
      {
         $sql = "FROM articulos WHERE ";
      }
      else if($this->subfamilias)
      {
         $sql = "FROM articulos WHERE codfamilia IN (";
         $coma = '';
         foreach($this->get_subfamilias($this->codfamilia) as $fam)
         {
            $sql .= $coma.$this->articulo->var2str($fam);
            $coma = ',';
         }
         $sql .= ") AND ";
      }
      else
         $sql = "FROM articulos WHERE codfamilia = ".$this->articulo->var2str($this->codfamilia)." AND ";

      if($this->codfabricante != '')
      {
         $sql .= "codfabricante = ".$this->articulo->var2str($this->codfabricante)." AND ";
      }

      if($this->codpscat != '')
      {
         $sql .= "referencia IN (SELECT referencia FROM ps_articuloscategoria WHERE idcat = "
                 .$this->articulo->var2str($this->codpscat).") AND ";
      }

      if($this->codproveedor != '')
      {
         $sql .= "referencia IN (SELECT referencia FROM articulosprov WHERE codproveedor = "
                 .$this->articulo->var2str($this->codproveedor).") AND ";
      }

      if($this->tipo == 'stock')
      {
         $sql .= "stockfis > 0 AND ";
      }
      else if($this->tipo == 'publico')
      {
         $sql .= "publico AND ";
      }
      else if($this->tipo == 'sevende')
      {
         $sql .= "sevende AND ";
      }
      else if($this->tipo == 'secompra')
      {
         $sql .= "secompra AND ";
      }
      else if($this->tipo == 'nofamilia')
      {
         $sql .= "codfamilia IS NULL AND ";
      }
      else if($this->tipo == 'nofabricante')
      {
         $sql .= "codfabricante IS NULL AND ";
      }

      if($query == '')
      {
         $sql .= "1 = 1";
      }
      else if( is_numeric($query) )
      {
         $sql .= "(referencia LIKE '%".$query."%' OR equivalencia LIKE '%".$query."%' OR descripcion LIKE '%".$query."%'
               OR codbarras = '".$query."')";
      }
      else
      {
         $buscar = str_replace(' ', '%', $query);
         $sql .= "(lower(referencia) LIKE '%".$buscar."%' OR lower(equivalencia) LIKE '%".$buscar."%'
               OR lower(descripcion) LIKE '%".$buscar."%')";
      }

      $data1 = $this->db->select("SELECT count(referencia) as num ".$sql);
      if($data1)
      {
         $this->num_resultados = intval($data1[0]['num']);

         $data2 = $this->db->select_limit("SELECT * ".$sql." ORDER BY referencia ASC", 500, $this->offset);
         if($data2)
         {
            foreach($data2 as $a)
            {
               $this->resultados[] = new articulo($a);
            }
         }
      }
   }

   private function aplicar_cambios()
   {
      $sql = "UPDATE articulos";
      $aplicar = FALSE;
      $coma = ' SET ';

      if( isset($_POST['stockmin']) )
      {
         if($_POST['n_codfamilia'] != '')
         {
            if($_POST['n_codfamilia'] == '#null#')
            {
               $sql .= $coma." codfamilia = NULL";
            }
            else
            {
               $sql .= $coma." codfamilia = ".$this->articulo->var2str($_POST['n_codfamilia']);
            }
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['n_codfabricante'] != '')
         {
            if($_POST['n_codfabricante'] == '#null#')
            {
               $sql .= $coma." codfabricante = NULL";
            }
            else
            {
               $sql .= $coma." codfabricante = ".$this->articulo->var2str($_POST['n_codfabricante']);
            }
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['stockfis'] != '')
         {
            $sql .= $coma." stockfis = ".$this->articulo->var2str($_POST['stockfis']);
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['stockmin'] != '')
         {
            $sql .= $coma." stockmin = ".$this->articulo->var2str($_POST['stockmin']);
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['stockmax'] != '')
         {
            $sql .= $coma." stockmax = ".$this->articulo->var2str($_POST['stockmax']);
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['controlstock'] != '')
         {
            $sql .= $coma." controlstock = ".$this->articulo->var2str($_POST['controlstock'] == '1');
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['nostock'] != '')
         {
            if($_POST['nostock'] == '1')
            {
               $sql .= $coma." stockfis = ".$this->articulo->var2str(0);
               $coma = ',';
               $aplicar = TRUE;
               $sql .= $coma." stockmin = ".$this->articulo->var2str(0);
               $sql .= $coma." stockmax = ".$this->articulo->var2str(0);
               $sql .= $coma." controlstock = ".$this->articulo->var2str(TRUE);
               $sql .= $coma." nostock = ".$this->articulo->var2str(TRUE);
            }
            else
            {
               $sql .= $coma." nostock = ".$this->articulo->var2str(FALSE);
               $coma = ',';
               $aplicar = TRUE;
            }
         }

         if($_POST['secompra'] != '')
         {
            $sql .= $coma." secompra = ".$this->articulo->var2str($_POST['secompra'] == '1');
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['sevende'] != '')
         {
            $sql .= $coma." sevende = ".$this->articulo->var2str($_POST['sevende'] == '1');
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['bloqueado'] != '')
         {
            $sql .= $coma." bloqueado = ".$this->articulo->var2str($_POST['bloqueado'] == '1');
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['publico'] != '')
         {
            $sql .= $coma." publico = ".$this->articulo->var2str($_POST['publico'] == '1');
            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['codimpuesto'] != '')
         {
            $sql .= $coma." codimpuesto = ".$this->articulo->var2str($_POST['codimpuesto']);
            $coma = ',';
            $aplicar = TRUE;
         }

         if( floatval($_POST['incpvp']) != 0 )
         {
            $sql .= $coma." pvp = (pvp*(100+".floatval($_POST['incpvp']).")/100)";
            $coma = ',';
            $aplicar = TRUE;
         }
         else if( floatval($_POST['inclpvp']) != 0 )
         {
            $sql .= $coma." pvp = (pvp+".floatval($_POST['inclpvp']).")";
            $coma = ',';
            $aplicar = TRUE;
         }

         if( floatval($_POST['inccoste']) != 0 )
         {
            if(FS_COST_IS_AVERAGE)
            {
               $sql .= $coma." costemedio = (costemedio*(100+".floatval($_POST['inccoste']).")/100)";
            }
            else
            {
               $sql .= $coma." preciocoste = (preciocoste*(100+".floatval($_POST['inccoste']).")/100)";
            }

            $coma = ',';
            $aplicar = TRUE;
         }
         else if( floatval($_POST['inclcoste']) != 0 )
         {
            if(FS_COST_IS_AVERAGE)
            {
               $sql .= $coma." costemedio = (costemedio+".floatval($_POST['inclcoste']).")";
            }
            else
            {
               $sql .= $coma." preciocoste = (preciocoste+".floatval($_POST['inclcoste']).")";
            }

            $coma = ',';
            $aplicar = TRUE;
         }

         if($_POST['margen'] != '')
         {
            if(FS_COST_IS_AVERAGE)
            {
               $sql .= $coma." pvp = (costemedio*(100+".floatval($_POST['margen']).")/100)";
            }
            else
            {
               $sql .= $coma." pvp = (preciocoste*(100+".floatval($_POST['margen']).")/100)";
            }

            $coma = ',';
            $aplicar = TRUE;
         }
      }

      $sql2 = '';
      if($this->codfamilia == '')
      {
         $sql2 .= " WHERE ";
      }
      else if($this->subfamilias)
      {
         $sql2 .= " WHERE codfamilia IN (";
         $coma = '';
         foreach($this->get_subfamilias($this->codfamilia) as $fam)
         {
            $sql2 .= $coma.$this->articulo->var2str($fam);
            $coma = ',';
         }
         $sql2 .= ") AND ";
      }
      else
         $sql2 .= " WHERE codfamilia = ".$this->articulo->var2str($this->codfamilia)." AND ";

      if($this->codfabricante != '')
      {
         $sql2 .= "codfabricante = ".$this->articulo->var2str($this->codfabricante)." AND ";
      }

      if($this->codpscat != '')
      {
         $sql2 .= "referencia IN (SELECT referencia FROM ps_articuloscategoria WHERE idcat = "
                 .$this->articulo->var2str($this->codpscat).") AND ";
      }

      if($this->codproveedor != '')
      {
         $sql2 .= "referencia IN (SELECT referencia FROM articulosprov WHERE codproveedor = "
                 .$this->articulo->var2str($this->codproveedor).") AND ";
      }

      if($this->tipo == 'stock')
      {
         $sql2 .= "stockfis > 0 AND ";
      }
      else if($this->tipo == 'publico')
      {
         $sql2 .= "publico AND ";
      }
      else if($this->tipo == 'sevende')
      {
         $sql2 .= "sevende AND ";
      }
      else if($this->tipo == 'secompra')
      {
         $sql2 .= "secompra AND ";
      }
      else if($this->tipo == 'nofamilia')
      {
         $sql2 .= "codfamilia IS NULL AND ";
      }

      $query = $this->articulo->no_html( strtolower($this->query) );
      if($query == '')
      {
         $sql2 .= "1 = 1";
      }
      else if( is_numeric($query) )
      {
         $sql2 .= "(referencia LIKE '%".$query."%' OR equivalencia LIKE '%".$query."%' OR descripcion LIKE '%".$query."%'
               OR codbarras = '".$query."')";
      }
      else
      {
         $buscar = str_replace(' ', '%', $query);
         $sql2 .= "(lower(referencia) LIKE '%".$buscar."%' OR lower(equivalencia) LIKE '%".$buscar."%'
               OR lower(descripcion) LIKE '%".$buscar."%')";
      }

      if($aplicar)
      {
         if( $this->db->exec($sql.$sql2.';') )
         {
            $data = $this->db->select("SELECT referencia FROM articulos".$sql2.";");
            if($data)
            {
               $this->aplicar_cambios_externos($data);
            }

            $this->new_message('Cambios aplicados correctamente.');
         }
         else
         {
            $this->new_error_msg('Error al aplicar los cambios.');
         }
      }
      else
      {
         /// esto es una chapuza para terminar de hacer cambios

         /// puede que haya que aplicar cambios en otras tablas
         $data = $this->db->select_limit("SELECT referencia FROM articulos".$sql2, 5000, 0);
         if($data)
         {
            $this->aplicar_cambios_externos($data);
         }
      }
   }

   private function get_subfamilias($cod)
   {
      $familias = array($cod);

      $data = $this->db->select("SELECT codfamilia,madre FROM familias WHERE madre = ".$this->familia->var2str($cod).";");
      if($data)
      {
         foreach($data as $d)
         {
            foreach($this->get_subfamilias($d['codfamilia']) as $subf)
            {
               $familias[] = $subf;
            }
         }
      }

      return $familias;
   }

   private function aplicar_cambios_externos($data)
   {
      $refs = array();
      foreach($data as $d)
      {
         $refs[] = $d['referencia'];
      }

      if( isset($_POST['nostock']) )
      {
         if($_POST['nostock'] == '1')
         {
            /// eliminamos el stock de los artículos nostock
            $this->db->exec("DELETE FROM stocks WHERE referencia IN (".join(',', $refs).");");
         }
      }

      if( isset($_POST['addpscat']) )
      {
         /// PrestaShop
         $cambios = FALSE;

         if($_POST['addpscat'] != '')
         {
            /// añadimos categorías
            foreach($refs as $ref)
            {
               $psartcat = new ps_articulo_categoria();
               $psartcat->referencia = $ref;
               $psartcat->idcat = intval($_POST['addpscat']);
               $psartcat->save();
               $cambios = TRUE;
            }
         }

         if($_POST['rmpscat'] != '')
         {
            /// quitamos categorías
            foreach($refs as $ref)
            {
               $psartcat = new ps_articulo_categoria();
               $psartcat->referencia = $ref;
               $psartcat->idcat = intval($_POST['rmpscat']);
               $psartcat->delete();
               $cambios = TRUE;
            }
         }

         if($cambios)
         {
            $this->new_message('Cambios para PrestaShop aplicados correctamente.'
                    . ' Se sincronizarán en segundo plano.');
         }
      }
   }
}
